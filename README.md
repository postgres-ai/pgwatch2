# pgwatch2 - Postgres.ai Edition

A modified version of [pgwatch2](https://github.com/cybertec-postgresql/pgwatch2), a popular monitoring tool developed by [Cybertec](https://www.cybertec-postgresql.com/en/).

Modifications:

- Improved dashboards for fast troubleshooting
- Custom metrics and configs
- Support of `pg_wait_sampling` and `pg_stat_kcache`
- Used [YAML based setup](https://pgwatch2.readthedocs.io/en/latest/custom_installation.html#yaml-based-setup)
- Metrics storage DB: [PostgreSQL](https://www.postgresql.org) with [timescaledb](https://www.timescale.com) extension (by default)

## Demo: <https://pgwatch.dblab.dev>

- username: `demo`
- password: `demo`

## How to configure and run

First, on Postgres hosts we want to observe:

- DB user that will be used by pgwatch2 + permissions it needs

```sql
CREATE ROLE pgwatch2 WITH LOGIN PASSWORD 'MY_SECRET_PASS';
GRANT pg_monitor TO pgwatch2;
GRANT EXECUTE ON FUNCTION pg_ls_dir(text) TO pgwatch2;  -- skip it if you have RDS/Aurora
GRANT EXECUTE ON FUNCTION pg_wait_sampling_reset_profile() TO pgwatch2; --if pg_wait_sampling extension is used
GRANT CONNECT ON DATABASE mydb TO pgwatch2;
GRANT USAGE ON SCHEMA public TO pgwatch2;
```

- Add pgwatch2 host to pg_hba.conf

### Extensions

For most monitored databases it’s extremely beneficial (to troubleshooting performance issues) to also activate  the pg_stat_statements extension:

- Adjust postgresql.conf

```properties
shared_preload_libraries = pg_stat_statements
```

- Restart postgres

- Create extension

```sql
create extension if not exists pg_stat_statements;
```

---

Or optional (can be omitted):

```bash
PG_VERSION=16; sudo apt install -y \
    postgresql-$PG_VERSION-pg-stat-kcache \
    postgresql-$PG_VERSION-pg-wait-sampling
```

- Adjust postgresql.conf

```properties
shared_preload_libraries = pg_stat_statements,pg_stat_kcache,pg_wait_sampling
```

- Restart postgres

- Create extensions

```sql
create extension if not exists pg_stat_statements;
create extension if not exists pg_stat_kcache;
create extension if not exists pg_wait_sampling;
```

---

### Instances

Copy and edit the `instances.yaml` configuration file (specify hosts you want to monitor):

```bash
sudo mkdir -p /etc/pgwatch2/config
sudo curl https://gitlab.com/postgres-ai/pgwatch2/-/raw/master/config/instances.yaml \
  --output /etc/pgwatch2/config/instances.yaml

sudo nano /etc/pgwatch2/config/instances.yaml
```

### Preset metrics

#### stat_statements_reset

In some cases, when queries executed on the postgres server are difficult to parameterize (ORM, ETL, dynamic sql, etc.), statistics accumulate a lot of information about queries that haven't been called for a long time.

It is useful to clean them periodically by calling the function ```pg_stat_statements_reset()``` keeping statistics up to date.

You can add the parameter ```stat_statements_reset``` to the preset that you are using, specifying the number of seconds as the interval value to enable automatic statistics reset functionality by pgwatch2 daemon.

Example:

```yml
- name: standard
  description: basic level + table, index, stat_statements stats
  metrics:
    stat_statements: 180
    stat_statements_reset: 604800 # every 1 week
    table_stats: 300
<..>
```

### Docker

```bash
sudo docker run -d --name pgwatch2-postgresai \
  -p 3000:3000 -p 8081:8081 \
  -v /etc/pgwatch2/config:/etc/pgwatch2/config:ro \
  -v pgwatch2:/etc/pgwatch2/persistent-config \
  -v pgwatch2_postgres:/var/lib/postgresql \
  -v pgwatch2_grafana:/var/lib/grafana \
  -e PW2_GRAFANANOANONYMOUS=true \
  -e PW2_GRAFANAUSER="admin" \
  -e PW2_GRAFANAPASSWORD="MY_SECRET_PASS" \
  -e PW2_DATASTORE="postgres" \
  -e PW2_PG_SCHEMA_TYPE="timescale" \
  -e PW2_PG_RETENTION_DAYS=14 \
  -e PW2_TIMESCALE_CHUNK_HOURS=1 \
  -e PW2_TIMESCALE_COMPRESS_HOURS=1 \
  --restart=unless-stopped \
  --shm-size=2g \
  postgresai/pgwatch2:1.12.0-4
```

### Dashboards

To view the dashboards, navigate to `http://<server-ip>:3000`

## Troubleshooting

Log into the container and look at log files - they’re situated under `/var/log/supervisor/`

Example:

```bash
sudo docker exec pgwatch2-postgresai ls -lh /var/log/supervisor/
sudo docker exec pgwatch2-postgresai tail -n 50 /var/log/supervisor/pgwatch2-stderr---supervisor-21oji6p3.log
```

## Compatibility with RDS/Aurora

Most metrics are compatible with AWS RDS and Aurora, with the exception of [wal_count](./metrics/wal_count/10/metric.sql) and [archiver_pending_count](./metrics/archiver_pending_count/10/metric.sql) metrics, because these PostgreSQL versions restrict access to the `pg_ls_dir` function. This means that charts such as, `WAL count` and `WAL Archive (pending_count)` will not be available. \
In addition, the [wal](./metrics/wal/10/metric.sql) metric (required for `WAL rate` chart) works in Aurora only if the `wal_level` parameter is set to `logical`. This is because the `pg_current_wal_lsn` function does not work in Aurora otherwise.
