SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  count(*) as archiver_pending_count
FROM 
  (SELECT pg_ls_dir('pg_xlog/archive_status')) a 
WHERE
  pg_ls_dir ~ '[0-9A-F]{24}.ready';
