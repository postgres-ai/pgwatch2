SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  CASE pg_is_in_recovery() WHEN 't' then 2 
ELSE (select case (select count(*) from pg_stat_replication where application_name != 'pg_basebackup') when '0' then 0 else 1 end)
END AS in_recovery_int;
