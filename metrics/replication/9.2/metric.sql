SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  application_name as tag_application_name,
  coalesce(pg_xlog_location_diff(case when pg_is_in_recovery() then pg_last_xlog_receive_location() else pg_current_xlog_location() end, sent_location)::int8, 0) as sent,          
  coalesce(pg_xlog_location_diff(sent_location, write_location)::int8, 0) as write,
  coalesce(pg_xlog_location_diff(write_location, flush_location)::int8, 0) as flush,
  coalesce(pg_xlog_location_diff(flush_location, replay_location)::int8, 0) as replay,
  coalesce(pg_xlog_location_diff(case when pg_is_in_recovery() then pg_last_xlog_receive_location() else pg_current_xlog_location() end, replay_location)::int8, 0) as total_lag,
  coalesce(client_addr::text, client_hostname) as tag_client_info,
  state
FROM pg_stat_replication;