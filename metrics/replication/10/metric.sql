SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  application_name as tag_application_name,
  coalesce(pg_wal_lsn_diff(case when pg_is_in_recovery() then pg_last_wal_receive_lsn() else pg_current_wal_lsn() end, sent_lsn)::int8, 0) as sent,          
  coalesce(pg_wal_lsn_diff(sent_lsn, write_lsn)::int8, 0) as write,
  coalesce(pg_wal_lsn_diff(write_lsn, flush_lsn)::int8, 0) as flush,
  coalesce(pg_wal_lsn_diff(flush_lsn, replay_lsn)::int8, 0) as replay,
  coalesce(pg_wal_lsn_diff(case when pg_is_in_recovery() then pg_last_wal_receive_lsn() else pg_current_wal_lsn() end, replay_lsn)::int8, 0) as total_lag,
  coalesce(client_addr::text, client_hostname) as tag_client_info,
  state
FROM pg_stat_replication;