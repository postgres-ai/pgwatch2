with sa_snapshot as ( /* pgwatch2_generated */
  select * from pg_stat_activity
  where pid != pg_backend_pid()
  and datname = current_database()
)
select
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  current_setting('max_connections')::int as max_connections,
  (select count(*) from sa_snapshot) as total,
  (select count(*) from sa_snapshot where backend_type = 'background worker') as background_workers,
  (select count(*) from sa_snapshot where state = 'active' and backend_type = 'client backend') as active,
  (select count(*) from sa_snapshot where state = 'idle' and backend_type = 'client backend') as idle,
  (select count(*) from sa_snapshot where state = 'idle in transaction' and backend_type = 'client backend') as idleintransaction,
  (select count(*) from sa_snapshot where wait_event_type in ('LWLock', 'Lock', 'BufferPin') and backend_type = 'client backend') as waiting,
  (select ceil(extract(epoch from max(now() - query_start)))::int from sa_snapshot where wait_event_type in ('LWLock', 'Lock', 'BufferPin') and backend_type = 'client backend') as longest_waiting_seconds,
  (select extract(epoch from (now() - backend_start))::int from sa_snapshot where backend_type = 'client backend' order by backend_start limit 1) as longest_session_seconds,
  (select extract(epoch from (now() - xact_start))::int from sa_snapshot where xact_start is not null and backend_type = 'client backend' and query not ilike 'ANALYZE %' and query not ilike 'VACUUM %'  order by xact_start limit 1) as longest_tx_seconds,
  (select extract(epoch from max(now() - query_start))::int from sa_snapshot where state = 'active' and backend_type = 'client backend' and query not ilike 'ANALYZE %' and query not ilike 'VACUUM %') as longest_query_seconds,
  (select age(txid_snapshot_xmin(txid_current_snapshot())::text::xid)) as xmin_age;
