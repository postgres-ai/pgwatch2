with ranked_tables as ( /* pgwatch2_generated */
  select
    row_number() over (order by c.reltuples desc nulls last) as rownum,
    quote_ident(nspname)::text as tag_schema,
    quote_ident(relname)::text as tag_table_name,
    (quote_ident(nspname) || '.' || quote_ident(relname))::text as tag_table_full_name,
    coalesce(c.reltuples, 0)::int8 as row_count
  from pg_class c
  left join pg_namespace n on (n.oid = c.relnamespace)
  where
    nspname not in ('pg_catalog', 'information_schema')
    and relkind = 'r'
)
select
  *,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_tables
where rownum <= 100
union all
select
  -1::int as rownum, /* other */
  'other'::text as tag_schema,
  'other'::text as tag_table_name,
  'other'::text as tag_table_full_name,
  sum(row_count)::int8 as row_count,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_tables
where rownum > 100;
