SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  1 as result
FROM ( select pg_wait_sampling_reset_profile()) as pg_wait_sampl_reset;
