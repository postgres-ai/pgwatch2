with ranked_functions as ( /* pgwatch2_generated */
  select
    row_number() over (order by total_time desc nulls last) as rownum,
    schemaname::text as tag_schema,
    funcname::text as tag_function_name,
    (quote_ident(schemaname) || '.' || quote_ident(funcname))::text as tag_function_full_name,
    p.oid::oid as tag_oid,
    coalesce(calls, 0)::int8 as sp_calls,
    coalesce(self_time, 0)::double precision as self_time,
    coalesce(total_time, 0)::double precision as total_time
  from pg_stat_user_functions f
    join pg_proc p on p.oid = f.funcid
)
select
  *,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_functions
where rownum <= 100
union all
select
  -1::int as rownum, /* other */
  'other'::text as tag_schema,
  'other'::text as tag_function_name,
  'other'::text as tag_function_full_name,
  null::oid as tag_oid,
  sum(sp_calls)::int8 as sp_calls,
  sum(self_time)::double precision as self_time,
  sum(total_time)::double precision as total_time,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_functions
where rownum > 100;
