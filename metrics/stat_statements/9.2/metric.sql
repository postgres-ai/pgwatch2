with q_data as ( /* pgwatch2_generated */
  select
    (regexp_replace(md5(query::varchar(1000)), E'\\D', '', 'g'))::varchar(10)::text as tag_queryid,
    max(query::varchar(8000)) as query,
    array_to_string(array_agg(distinct quote_ident(pg_get_userbyid(userid))), ',') as users,
    sum(s.calls)::int8 as calls,
    round(sum(s.total_time)::numeric, 3)::double precision as total_time,
    sum(s.rows)::int8 as rows,
    sum(shared_blks_hit)::int8 as shared_blks_hit,
    sum(shared_blks_read)::int8 as shared_blks_read,
    sum(shared_blks_written)::int8 as shared_blks_written,
    sum(shared_blks_dirtied)::int8 as shared_blks_dirtied,
    sum(shared_blks_hit)::int8 * current_setting('block_size')::int as shared_blks_hit_bytes,
    sum(shared_blks_read)::int8 * current_setting('block_size')::int as shared_blks_read_bytes,
    sum(shared_blks_written)::int8 * current_setting('block_size')::int as shared_blks_written_bytes,
    sum(shared_blks_dirtied)::int8 * current_setting('block_size')::int as shared_blks_dirtied_bytes,
    sum(temp_blks_read)::int8 * current_setting('block_size')::int as temp_blks_read_bytes,
    sum(temp_blks_written)::int8 * current_setting('block_size')::int as temp_blks_written_bytes,
    sum(temp_blks_read)::int8 as temp_blks_read,
    sum(temp_blks_written)::int8 as temp_blks_written,
    round(sum(blk_read_time)::numeric, 3)::double precision as blk_read_time,
    round(sum(blk_write_time)::numeric, 3)::double precision as blk_write_time
  from
    pg_stat_statements s
  where
    dbid = (
      select
        oid
      from
        pg_database
      where
        datname = current_database())
      and not upper(s.query::varchar(50))
      ilike any (array['deallocate%',
        'set %',
        'reset %',
        'begin%',
        'commit%',
        'end%',
        'rollback%',
        'show%',
        'discard all'])
  group by
    tag_queryid
),
combined_data as (
  select
    (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
    b.tag_queryid,
    b.users,
    b.calls,
    b.total_time,
    b.rows,
    b.shared_blks_hit,
    b.shared_blks_read,
    b.shared_blks_written,
    b.shared_blks_dirtied,
    b.shared_blks_hit_bytes,
    b.shared_blks_read_bytes,
    b.shared_blks_written_bytes,
    b.shared_blks_dirtied_bytes,
    b.temp_blks_read_bytes,
    b.temp_blks_written_bytes,
    b.temp_blks_read,
    b.temp_blks_written,
    b.blk_read_time,
    b.blk_write_time,
    ltrim(regexp_replace(b.query, e'[ \\t\\n\\r"\'`]+', ' ', 'g')) as tag_query
  from (
    select *
    from (
      select *
      from q_data
      where total_time > 0
      order by total_time desc nulls last
      limit 100) a
    union
    select *
    from (
      select *
      from q_data
      order by calls desc nulls last
      limit 100) a
    union
    select *
    from (
      select *
      from q_data
      where shared_blks_read > 0
      order by shared_blks_read desc nulls last
      limit 100) a
    union
    select *
    from (
      select *
      from q_data
      where shared_blks_hit > 0
      order by shared_blks_hit desc nulls last
      limit 100) a
    union
    select *
    from (
      select *
      from q_data
      where shared_blks_written > 0
      order by shared_blks_written desc nulls last
      limit 100) a
    union
    select *
    from (
      select *
      from q_data
      where temp_blks_read > 0
      order by temp_blks_read desc nulls last
      limit 100) a
    union
    select *
    from (
      select *
      from q_data
      where temp_blks_written > 0
      order by temp_blks_written desc nulls last
      limit 100) a
  ) b
),
other_data as (
  select
    (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
    '-1'::text as tag_queryid,
    'other' as users,
    sum(calls)::int8 as calls,
    round(sum(total_time)::numeric, 3)::double precision as total_time,
    sum(rows)::int8 as rows,
    sum(shared_blks_hit)::int8 as shared_blks_hit,
    sum(shared_blks_read)::int8 as shared_blks_read,
    sum(shared_blks_written)::int8 as shared_blks_written,
    sum(shared_blks_dirtied)::int8 as shared_blks_dirtied,
    sum(shared_blks_hit_bytes)::int8 as shared_blks_hit_bytes,
    sum(shared_blks_read_bytes)::int8 as shared_blks_read_bytes,
    sum(shared_blks_written_bytes)::int8 as shared_blks_written_bytes,
    sum(shared_blks_dirtied_bytes)::int8 as shared_blks_dirtied_bytes,
    sum(temp_blks_read_bytes)::int8 as temp_blks_read_bytes,
    sum(temp_blks_written_bytes)::int8 as temp_blks_written_bytes,
    sum(temp_blks_read)::int8 as temp_blks_read,
    sum(temp_blks_written)::int8 as temp_blks_written,
    round(sum(blk_read_time)::numeric, 3)::double precision as blk_read_time,
    round(sum(blk_write_time)::numeric, 3)::double precision as blk_write_time,
    'other' as query
  from q_data q
  where not exists (
    select 1
    from combined_data cd
    where q.tag_queryid = cd.tag_queryid
  )
)
select *
from combined_data
union all
select *
from other_data;
