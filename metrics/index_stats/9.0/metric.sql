with ranked_indexes as ( /* pgwatch2_generated */
  select
    row_number() over (order by idx_scan desc nulls last) as rownum,
    schemaname::text as tag_schema,
    indexrelname::text as tag_index_name,
    (quote_ident(schemaname) || '.' || quote_ident(indexrelname))::text as tag_index_full_name,
    relname::text as tag_table_name,
    (quote_ident(schemaname) || '.' || quote_ident(relname))::text as tag_table_full_name,
    coalesce(idx_scan, 0)::int8 as idx_scan,
    coalesce(idx_tup_read, 0)::int8 as idx_tup_read,
    coalesce(idx_tup_fetch, 0)::int8 as idx_tup_fetch,
    coalesce(pg_relation_size(indexrelid), 0)::int8 as index_size_b,
    (quote_ident(schemaname) || '.' || quote_ident(indexrelname))::text as index_full_name_val,
    coalesce(md5(regexp_replace(replace(pg_get_indexdef(indexrelid), indexrelname, 'X'), '^CREATE UNIQUE', 'CREATE')), '')::text as tag_index_def_hash,
    coalesce(regexp_replace(replace(pg_get_indexdef(indexrelid), indexrelname, 'X'), '^CREATE UNIQUE', 'CREATE'), '')::text as index_def,
    (case when not i.indisvalid then 1 else 0 end)::int as is_invalid_int,
    (case when i.indisprimary then 1 else 0 end)::int as is_pk_int
  from pg_stat_user_indexes sui
    join pg_index i using (indexrelid)
  where not schemaname like E'pg\\_temp%'
)
select
  *,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_indexes
where rownum <= 100
union all
select
  -1::int as rownum, /* other */
  'other'::text as tag_schema,
  'other'::text as tag_index_name,
  'other'::text as tag_index_full_name,
  'other'::text as tag_table_name,
  'other'::text as tag_table_full_name,
  sum(idx_scan)::int8 as idx_scan,
  sum(idx_tup_read)::int8 as idx_tup_read,
  sum(idx_tup_fetch)::int8 as idx_tup_fetch,
  sum(index_size_b)::int8 as index_size_b,
  ''::text as index_full_name_val,
  ''::text as tag_index_def_hash,
  ''::text as index_def,
  0::int as is_invalid_int,
  0::int as is_pk_int,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_indexes
where rownum > 100;
