select /* pgwatch2_generated */
 (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
 coalesce(sum(calls), 0)::int8 as calls
from
 pg_stat_statements
where
 dbid = (select oid from pg_database where datname = current_database());
