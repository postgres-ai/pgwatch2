with ranked_kcache as ( /* pgwatch2_generated */
  select
    row_number() over (order by user_time desc nulls last, system_time desc nulls last) as rownum,
    queryid::text as tag_queryid,
    coalesce(user_time, 0)::double precision as user_time,
    coalesce(system_time, 0)::double precision as system_time,
    coalesce(minflts, 0)::int8 as minflts,
    coalesce(majflts, 0)::int8 as majflts,
    coalesce(reads, 0)::int8 as reads,
    coalesce(writes, 0)::int8 as writes,
    coalesce(nvcsws, 0)::int8 as nvcsws,
    coalesce(nivcsws, 0)::int8 as nivcsws
  from pg_stat_kcache()
  where dbid = (select oid from pg_database where datname = current_database())
)
select
  *,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_kcache
where rownum <= 100
union all
select
  -1::int as rownum, /* other */
  '-1'::text as tag_queryid,
  sum(user_time)::double precision as user_time,
  sum(system_time)::double precision as system_time,
  sum(minflts)::int8 as minflts,
  sum(majflts)::int8 as majflts,
  sum(reads)::int8 as reads,
  sum(writes)::int8 as writes,
  sum(nvcsws)::int8 as nvcsws,
  sum(nivcsws)::int8 as nivcsws,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_kcache
where rownum > 100;
