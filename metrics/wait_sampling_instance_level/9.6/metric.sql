SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns, 
  event_type as tag_wait_type, 
  event as tag_wait_event, 
  sum(count)::int8 as of_events 
FROM pg_wait_sampling_profile
GROUP BY event_type, event
ORDER BY of_events desc;
