with ranked_tables as ( /* pgwatch2_generated */
  select
    row_number() over (order by pg_total_relation_size(relid) desc nulls last) as rownum,
    quote_ident(schemaname)::text as tag_schema,
    quote_ident(relname)::text as tag_table_name,
    (quote_ident(schemaname) || '.' || quote_ident(relname))::text as tag_table_full_name,
    pg_relation_size(relid)::double precision as table_size_b,
    pg_total_relation_size(relid)::double precision as total_relation_size_b,
    coalesce(pg_relation_size((select reltoastrelid from pg_class where oid = ut.relid)), 0)::double precision as toast_size_b,
    extract(epoch from now() - greatest(last_vacuum, last_autovacuum))::int8 as seconds_since_last_vacuum,
    extract(epoch from now() - greatest(last_analyze, last_autoanalyze))::int8 as seconds_since_last_analyze,
    coalesce(seq_scan, 0)::int8 as seq_scan,
    coalesce(seq_tup_read, 0)::int8 as seq_tup_read,
    coalesce(idx_scan, 0)::int8 as idx_scan,
    coalesce(idx_tup_fetch, 0)::int8 as idx_tup_fetch,
    coalesce(n_tup_ins, 0)::int8 as n_tup_ins,
    coalesce(n_tup_upd, 0)::int8 as n_tup_upd,
    coalesce(n_tup_del, 0)::int8 as n_tup_del,
    coalesce(n_tup_hot_upd, 0)::int8 as n_tup_hot_upd,
    coalesce(n_dead_tup, 0)::int8 as n_dead_tup,
    coalesce(vacuum_count, 0)::int8 as vacuum_count,
    coalesce(autovacuum_count, 0)::int8 as autovacuum_count,
    coalesce(analyze_count, 0)::int8 as analyze_count,
    coalesce(autoanalyze_count, 0)::int8 as autoanalyze_count
  from
    pg_stat_user_tables ut
  where
    not exists (select 1 from pg_locks where relation = relid and mode = 'AccessExclusiveLock' and granted)
)
select
  *,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_tables
where rownum <= 100
union all
select
  -1::int as rownum, /* other */
  'other'::text as tag_schema,
  'other'::text as tag_table_name,
  'other'::text as tag_table_full_name,
  sum(table_size_b)::double precision as table_size_b,
  sum(total_relation_size_b)::double precision as total_relation_size_b,
  sum(toast_size_b)::double precision as toast_size_b,
  0::int8 as seconds_since_last_vacuum,
  0::int8 as seconds_since_last_analyze,
  sum(seq_scan)::int8 as seq_scan,
  sum(seq_tup_read)::int8 as seq_tup_read,
  sum(idx_scan)::int8 as idx_scan,
  sum(idx_tup_fetch)::int8 as idx_tup_fetch,
  sum(n_tup_ins)::int8 as n_tup_ins,
  sum(n_tup_upd)::int8 as n_tup_upd,
  sum(n_tup_del)::int8 as n_tup_del,
  sum(n_tup_hot_upd)::int8 as n_tup_hot_upd,
  sum(n_dead_tup)::int8 as n_dead_tup,
  sum(vacuum_count)::int8 as vacuum_count,
  sum(autovacuum_count)::int8 as autovacuum_count,
  sum(analyze_count)::int8 as analyze_count,
  sum(autoanalyze_count)::int8 as autoanalyze_count,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_tables
where rownum > 100;
