SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  age(datfrozenxid) as datfrozenxid_age 
FROM 
  pg_database 
WHERE 
  datname = current_database();
