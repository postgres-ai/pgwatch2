with ranked_indexes as ( /* pgwatch2_generated */
  select
    row_number() over (order by idx_scan desc nulls last) as rownum,
    relname::text as tag_table_name,
    indexrelname::text as tag_index_name,
    coalesce(idx_scan, 0)::int8 as index_scans
  from pg_stat_user_indexes
    join pg_index
    using (indexrelid)
  where indisunique is false
)
select
  *,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_indexes
where rownum <= 100
union all
select
  -1 as rownum, /* other */
  'other'::text as tag_table_name,
  'other'::text as tag_index_name,
  sum(index_scans)::int8 as index_scans,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_indexes
where rownum > 100;
