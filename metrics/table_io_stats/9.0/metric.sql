with ranked_tables as ( /* pgwatch2_generated */
  select
    row_number() over (order by heap_blks_read desc nulls last) as rownum,
    schemaname::text as tag_schema,
    relname::text as tag_table_name,
    (quote_ident(schemaname) || '.' || quote_ident(relname))::text as tag_table_full_name,
    coalesce(heap_blks_read, 0)::int8 as heap_blks_read,
    coalesce(heap_blks_hit, 0)::int8 as heap_blks_hit,
    coalesce(idx_blks_read, 0)::int8 as idx_blks_read,
    coalesce(idx_blks_hit, 0)::int8 as idx_blks_hit,
    coalesce(toast_blks_read, 0)::int8 as toast_blks_read,
    coalesce(toast_blks_hit, 0)::int8 as toast_blks_hit,
    coalesce(tidx_blks_read, 0)::int8 as tidx_blks_read,
    coalesce(tidx_blks_hit, 0)::int8 as tidx_blks_hit
  from pg_statio_user_tables
  where
    not schemaname like E'pg\\_temp%'
    and (heap_blks_read > 0 or heap_blks_hit > 0 or idx_blks_read > 0 or idx_blks_hit > 0 or tidx_blks_read > 0 or tidx_blks_hit > 0)
)
select
  *,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_tables
where rownum <= 100
union all
select
  -1::int as rownum, /* other */
  'other'::text as tag_schema,
  'other'::text as tag_table_name,
  'other'::text as tag_table_full_name,
  sum(heap_blks_read)::int8 as heap_blks_read,
  sum(heap_blks_hit)::int8 as heap_blks_hit,
  sum(idx_blks_read)::int8 as idx_blks_read,
  sum(idx_blks_hit)::int8 as idx_blks_hit,
  sum(toast_blks_read)::int8 as toast_blks_read,
  sum(toast_blks_hit)::int8 as toast_blks_hit,
  sum(tidx_blks_read)::int8 as tidx_blks_read,
  sum(tidx_blks_hit)::int8 as tidx_blks_hit,
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns
from ranked_tables
where rownum > 100;
