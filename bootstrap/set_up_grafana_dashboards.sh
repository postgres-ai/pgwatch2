#!/bin/bash

if [[ -f /etc/pgwatch2/persistent-config/grafana-bootstrap-done-marker ]]; then
  exit 0
fi

declare -i attempts=$(( 60 / 5 ))
while [[ "$(curl -X GET -s -o /dev/null -w "%{http_code}" http://localhost:3000/api/health)" != "200" ]]; do
  echo "grafana server is not available"
  if (( $((attempts -= 1)) == 0 )); then
    exit 1
  fi
  sleep 5
done

timescaledb_value="false"
if [[ "${PW2_PG_SCHEMA_TYPE}" == "timescale" ]]; then
  timescaledb_value="true"
fi

# PostgreSQL 11+ only supported
if [[ "${POSTGRES_VERSION}" -eq 11 ]]; then
  postgresVersion_value="1100"
elif [[ "${POSTGRES_VERSION}" -gt 11 ]]; then
  postgresVersion_value="1200"
else
  exit 1
fi

# Create datasource
echo "creating datasource: pg-metrics"
echo "{
    \"orgId\": 1,
    \"name\": \"PG metrics\",
    \"uid\": \"pg-metrics\",
    \"type\": \"postgres\",
    \"access\": \"proxy\",
    \"url\": \"localhost:5432\",
    \"user\": \"pgwatch2\",
    \"database\": \"pgwatch2_metrics\",
    \"basicAuth\": false,
    \"isDefault\": true,
    \"jsonData\": {
        \"postgresVersion\": ${postgresVersion_value},
        \"sslmode\": \"disable\",
        \"timescaledb\": ${timescaledb_value}
    },
    \"secureJsonData\": {
        \"password\": \"pgwatch2admin\"
    },
    \"version\": 0,
    \"readOnly\": false
}" | \
curl -X POST -s -w "%{http_code}" \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -u "${PW2_GRAFANAUSER:-admin}":"${PW2_GRAFANAPASSWORD:-admin}" \
  -d @- \
  'http://localhost:3000/api/datasources'

# Create dashboards
GRAFANA_MAJOR_VER=$(echo $GRAFANA_VERSION | cut -d '.' -f 1)

for dashboard_json in $(find /etc/pgwatch2/grafana-dashboards/postgres/v"${GRAFANA_MAJOR_VER}" -name "*.json" | sort); do
  echo
  echo "creating dashboard: ${dashboard_json}"
  echo "{
      \"dashboard\": $(cat "${dashboard_json}"),
      \"message\": \"Bootstrap dashboard\",
      \"overwrite\": false,
      \"inputs\": [{
              \"name\": \"DS_PG_METRICS\",
              \"type\": \"datasource\",
              \"pluginId\": \"postgres\",
              \"value\": \"pg-metrics\"
    }]
  }" | \
  curl -X POST -s -w "%{http_code}" \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -u "${PW2_GRAFANAUSER:-admin}":"${PW2_GRAFANAPASSWORD:-admin}" \
    -d @- \
    'http://localhost:3000/api/dashboards/import'
done

# Set home dashboard
# Let's make the assumption that due to the loading of dashboards in previous step in alphabetical order,
# the "Global DB overview" dashboard will always be under id=1.
# Otherwise, we can install the jq and get the identifier through the api:
#   curl -H 'Accept: application/json' 'http://localhost:3000/api/dashboards/uid/global-db-overview' | jq '.dashboard.id'
echo
echo "starring dashboard global-db-overview"
curl -X POST -s -w "%{http_code}" \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -u "${PW2_GRAFANAUSER:-admin}":"${PW2_GRAFANAPASSWORD:-admin}" \
  'http://localhost:3000/api/user/stars/dashboard/1'

echo
echo "set global-db-overview as home page"
echo "{\"homeDashboardId\": 1}" | \
curl -X PATCH -s -w "%{http_code}" \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -u "${PW2_GRAFANAUSER:-admin}":"${PW2_GRAFANAPASSWORD:-admin}" \
  -d @- \
  'http://localhost:3000/api/org/preferences'

touch /etc/pgwatch2/persistent-config/grafana-bootstrap-done-marker

exit 0
