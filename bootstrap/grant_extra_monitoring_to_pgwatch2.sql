CREATE EXTENSION IF NOT EXISTS pg_stat_statements;
CREATE EXTENSION IF NOT EXISTS pg_wait_sampling;
CREATE EXTENSION IF NOT EXISTS pg_stat_kcache;

GRANT EXECUTE ON FUNCTION pg_stat_file(text) to pgwatch2;
GRANT EXECUTE ON FUNCTION pg_ls_dir(text) TO pgwatch2;
GRANT EXECUTE ON FUNCTION pg_wait_sampling_reset_profile() TO pgwatch2;
GRANT EXECUTE ON FUNCTION pg_stat_statements_reset() TO pgwatch2;
